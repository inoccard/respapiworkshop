﻿namespace RestApiWorkshop.Api.Models
{
    public class TagDto
    {
        public int Id { get; set; }

        public string Value { get; set; }
    }
}