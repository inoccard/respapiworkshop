﻿using RestApiWorkshop.Api.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RestApiWorkshop.Api.DTO
{
    public class BlogDto
    {
        [Key]
        public int Id { get; set; }

        public string Url { get; set; }

        public List<PostDTO> Posts { get; set; } = new List<PostDTO>();

        public AuthorDto Author { get; set; }
    }
}
