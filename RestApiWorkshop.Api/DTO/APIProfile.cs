﻿using AutoMapper;
using RestApiWorkshop.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestApiWorkshop.Api.DTO
{
    public class APIProfile : Profile
    {
        public APIProfile()
        {
            CreateMap<Author, AuthorDto>()
            .ReverseMap();
            CreateMap<Blog, BlogDto>()
                .ReverseMap();
            CreateMap<Post, PostDTO>()
                .ReverseMap();
            CreateMap<Tag, TagDto>()
                .ReverseMap();
        }
    }
}
