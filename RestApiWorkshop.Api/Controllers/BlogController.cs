﻿using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RestApiWorkshop.Api.Data;
using RestApiWorkshop.Api.DTO;
using RestApiWorkshop.Api.Models;

namespace RestApiWorkshop.Api.Controllers
{
    [ApiController]
    [Route("blogs")]
    public class BlogController : ControllerBase
    {
        private readonly IMyDatabase myDatabase;
        private readonly IMapper mapper;

        public BlogController(IMyDatabase myDatabase, IMapper mapper)
        {
            this.myDatabase = myDatabase;
            this.mapper = mapper;
        }
        /// <summary>
        /// Busca um único blog
        /// </summary>
        /// <remarks>
        /// Forma de utilizar:
        /// GET /blogs?filter=Eu&skip=10&top=15
        /// </remarks>
        /// <param name="filter">utilizado para filtrar o nome do autor</param>
        /// <param name="top">utilizado para buscar apenas um número específico de registro</param>
        /// <param name="skip">utilizado para pular um egistro</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesDefaultResponseType(typeof(BlogDto[]))] // para demonstrar o uso do esquema 
        public IActionResult Get([FromQuery] string filter = null, [FromQuery] int top = 100, [FromQuery] int skip = 0)
        {
            if (filter is null)
                filter = string.Empty;

            var blog = myDatabase.Blogs.
                Include(a => a.Author)
                .Include(a => a.Posts)
                .ThenInclude(a => a.Tags)
                .Where(a => a.Author.Name == string.Empty || a.Author.Name.Contains(filter))
                .Skip(skip) /*pula isto*/
                .Take(top) // limita a 100 registros
                 .ProjectTo<BlogDto>(mapper.ConfigurationProvider);

            if (blog is null)
                return NotFound();

            return Ok(blog);
        }

        [HttpGet("{blogId}")]
        public IActionResult GetById([FromRoute]int blogId)
        {
            var blog = myDatabase.Blogs.
                Include(a => a.Author)
                .Include(a => a.Posts)
                .ThenInclude(a => a.Tags)
                .SingleOrDefault(a => a.Id == blogId);

            if (blog is null)
                return NotFound();
            // mapeia Dto
            var dto = mapper.Map<BlogDto>(blog);

            return Ok(dto);
        }

        [HttpPost]
        public IActionResult Post([FromBody] Blog blog)
        {
            blog.Id = myDatabase.Blogs.Max(a => a.Id) + 1;
            myDatabase.Blogs.Add(blog);
            myDatabase.SaveChangesAsync();
            return CreatedAtAction(nameof(GetById), new {id = blog.Id });
        }
        
        [HttpPost]
        public async Task<IActionResult> Post2([FromBody] BlogDto blog)
        {
            var newBlog = mapper.Map<Blog>(blog);

            newBlog.Id = await myDatabase.Blogs.MaxAsync(a => a.Id)+1;
            myDatabase.Blogs.Add(newBlog);
            await myDatabase.SaveChangesAsync();
            return CreatedAtAction(nameof(GetById), new {id = newBlog.Id }, mapper.Map<Blog>(newBlog));
        }

        /// <summary>
        /// Put não é feito em cima da coleção, é feito no objeto
        /// </summary>
        /// <param name="id"></param>
        /// <param name="blog"></param>
        /// <returns></returns>
        [HttpPut("id")]
        public IActionResult Put([FromRoute] int id, [FromBody] Blog blog)
        {
            if (id != blog.Id)
                return BadRequest();
            var oldBlog = myDatabase.Blogs.Find(id);

            if (oldBlog is null)
                return NotFound();

            oldBlog.Url = blog.Url;

            myDatabase.SaveChanges();

            return NoContent();
            // return Ok(oldBlog);
        }

        /// <summary>
        /// Delete Trabalha em cima da rota
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public IActionResult Delete([FromRoute] int id)
        {
            var oldBlog = myDatabase.Blogs.Find(id);

            if (oldBlog is null)
                return NoContent();
            myDatabase.Blogs.Remove(oldBlog);
            myDatabase.SaveChanges();

            return NoContent();

        }

        [HttpGet("{id}/author")]
        public IActionResult GetAuthor([FromRoute] int id)
        {
            var blog = myDatabase.Blogs.Include(a => a.Author).SingleOrDefault(a => a.Id == id);
            if (blog is null)
                return NotFound();
            return Ok(blog.Author);
        }
        
        [HttpGet("{id}/posts")]
        public IActionResult GetPosts([FromRoute]int id, [FromQuery] string[] tags = null)
        {
            if (!myDatabase.Blogs.Any(a => a.Id == id))
                return NotFound();

            if (tags?.Length == 0)
                tags = null;

            var posts = myDatabase.Blogs.Include(a => a.Posts)
                .ThenInclude(a => a.Tags)
                .Where(a => a.Id == id)
                .SelectMany(a => a.Posts)
                .Where(a => tags == null || a.Tags.Any(b => tags.Contains(b.Value))).ToList();
            
            if (posts is null)
                return NotFound();

            return Ok(posts);
        }
        
        [HttpGet("{id}/postsasync")]
        public async Task<IActionResult> GetPosts2([FromRoute]int id, [FromQuery] string[] tags = null)
        {
            if (!await myDatabase.Blogs.AnyAsync(a => a.Id == id))
                return NotFound();

            if (tags?.Length == 0)
                tags = null;

            var posts = myDatabase.Blogs.Include(a => a.Posts)
                .ThenInclude(a => a.Tags)
                .Where(a => a.Id == id)
                .SelectMany(a => a.Posts)
                .Where(a => tags == null || a.Tags.Any(b => tags.Contains(b.Value))).ToListAsync();
            
            if (posts is null)
                return NotFound();

            return Ok(posts);
        }
    }
}