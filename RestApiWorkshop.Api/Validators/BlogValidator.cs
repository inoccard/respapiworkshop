﻿using FluentValidation;
using RestApiWorkshop.Api.DTO;

namespace RestApiWorkshop.Api.Validators
{
    /// <summary>
    /// Para validar entrada de dados
    /// </summary>
    public class BlogValidator : AbstractValidator<BlogDto>
    {
        /// <summary>
        /// Se algum campo for vazio, retornará bad request
        /// </summary>
        public BlogValidator()
        {
            RuleFor(a => a.Url).NotEmpty();
            RuleFor(a => a.Posts).Empty();
            RuleFor(a => a.Author).Null();
        }
    }
}
